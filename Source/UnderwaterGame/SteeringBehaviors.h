// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Math/Vector.h"
#include "Math/Vector2D.h"
#include "GameFramework/Character.h"

class Mover;

/**
 *	Various sterring behaviors to be used by the AI
 */
class UNDERWATERGAME_API SteeringBehavior
{
public:
	enum summing_method{ weighted_average, prioritized, dithered };

protected:

	enum behaviorType
	{
		none = 0x0000,
		seek = 0x0002,
		flee = 0x0004,
		arrive = 0x0008,
		pursuit = 0x0010,
		evade = 0x0020,
		interpose = 0x0040,
		wander = 0x0080,
		cohesion = 0x0100,
		separation = 0x0200,
		allignment = 0x0400,
		wanderyaw = 0x0800,
		wanderpitch = 0x1000,
		cohesionUnit = 0x2000,
		separationUnit = 0x4000,
		allignmentUnit = 0x8000
	};

	// The owner of this instance
	Mover* m_Owner;

	// The steering force combined from all activated steering behaviors
	FVector m_SteeringForce;

	Mover* m_TargetChar1;
	Mover* m_TargetChar2;

	FVector m_SeekTarget;
	FVector m_FleeTarget;

	FVector m_WanderTarget;

	float m_WanderJitter;
	float m_WanderRadius;
	float m_WanderDistance;

	float       m_WeightSeparation;
	float       m_WeightCohesion;
	float       m_WeightAlignment;
	float       m_WeightWander;
	float       m_WeightSeek;
	float       m_WeightFlee;
	float       m_WeightArrive;
	float       m_WeightPursuit;
	float       m_WeightInterpose;
	float       m_WeightEvade;
	float		m_WeightCohesionUnit;
	float		m_WeightSeparationUnit;
	float		m_WeightAlignmentUnit;

	int m_BehaviorFlags;

	enum Deceleration{ slow = 3, normal = 2, fast = 1 };

	Deceleration m_Deceleration;

	summing_method m_SummingMethod;

	// this function check if the bit corresponding to the behaviorType param is set to on
	bool On(behaviorType type){ return (m_BehaviorFlags & type) == type; }

	// Adds a force to the combined sterring force, 
	//  returns false if there is no more force available to add
	bool AccumulateForce(FVector &totalForce, FVector forceToAdd);

public:
	SteeringBehavior(Mover* owner);
	~SteeringBehavior();

	FVector Calculate();

	void SetSeekTarget(const FVector target){ m_SeekTarget = target; }
	void SetFleeTarget(const FVector target){ m_FleeTarget = target; }

	void SetTargetCharacter1(Mover* target){ m_TargetChar1 = target; }
	void SetTargetCharacter2(Mover* target){ m_TargetChar2 = target; }

	void SetSummingMethod(summing_method method){ m_SummingMethod = method; }

	// Function to verify that the Steering Force is valid and not an undefined value
	bool VerifyGoodForce(FVector &vector);

	// calculate steering forces based on summing method
	FVector CalculateWeightedSum();
	FVector CalculatePrioritized();
	FVector CalculateDithered();

public:
	// The following functions are used to set steering behaviors "On"
	void SeekOn(){ m_BehaviorFlags |= seek; }
	void FleeOn(){ m_BehaviorFlags |= flee; }
	void ArriveOn(){ m_BehaviorFlags |= arrive; }
	void PursuitOn(Mover* c){ m_BehaviorFlags |= pursuit; m_TargetChar1 = c; }
	void EvadeOn(Mover* c){ m_BehaviorFlags |= evade; m_TargetChar1 = c; }
	void InterposeOn(Mover* c1, Mover* c2){ m_BehaviorFlags |= interpose; m_TargetChar1 = c1; m_TargetChar2 = c2; }
	void WanderOn(){ m_BehaviorFlags |= wander; }
	void WanderYawOn(){ m_BehaviorFlags |= wanderyaw; }
	void WanderPitchOn(){ m_BehaviorFlags |= wanderpitch; }
	void CohesionOn(){ m_BehaviorFlags |= cohesion; }
	void SeparationOn(){ m_BehaviorFlags |= separation; }
	void AlignmentOn(){ m_BehaviorFlags |= allignment; }
	void FlockingOn(){ CohesionOn(); SeparationOn(); AlignmentOn(); }

	void CohesionUnitOn(){ m_BehaviorFlags |= cohesionUnit; }
	void SeparationUnitOn(){ m_BehaviorFlags |= separationUnit; }
	void AlignmentUnitOn(){ m_BehaviorFlags |= allignmentUnit; }
	void FlockingUnitOn(Mover* c){ CohesionUnitOn(); SeparationUnitOn(); AlignmentUnitOn(); m_TargetChar1 = c; }

	// The following functions are used to set steering behaviors "Off"
	void SeekOff(){ if (On(seek)) m_BehaviorFlags ^= seek; }
	void FleeOff(){ if (On(flee)) m_BehaviorFlags ^= flee; }
	void ArriveOff(){ if (On(arrive)) m_BehaviorFlags ^= arrive; }
	void PursuitOff(){ if (On(pursuit)) m_BehaviorFlags ^= pursuit; }
	void EvadeOff(){ if (On(evade)) m_BehaviorFlags ^= evade; }
	void InterposeOff(){ if (On(interpose)) m_BehaviorFlags ^= interpose; }
	void WanderOff(){ if (On(wander)) m_BehaviorFlags ^= wander; }
	void WanderYawOff(){ if (On(wanderyaw)) m_BehaviorFlags ^= wanderyaw; }
	void WanderPitchOff(){if(On(wanderpitch)) m_BehaviorFlags ^= wanderpitch;}
	void CohesionOff(){ if (On(cohesion)) m_BehaviorFlags ^= cohesion; }
	void SeparationOff(){ if (On(separation)) m_BehaviorFlags ^= separation; }
	void AlignmentOff(){ if (On(allignment)) m_BehaviorFlags ^= allignment; }
	void FlockingOff(){ CohesionOff(); SeparationOff(); AlignmentOff(); }

	void CohesionUnitOff(){ if (On(cohesionUnit)) m_BehaviorFlags ^= cohesionUnit; }
	void SeparationUnitOff(){ if (On(separationUnit)) m_BehaviorFlags ^= separationUnit; }
	void AlignmentUnitOff(){ if (On(allignmentUnit)) m_BehaviorFlags ^= allignmentUnit; }
	void FlockingUnitOff(){ CohesionUnitOff(); SeparationUnitOff(); AlignmentUnitOff(); }

	// The following functions return true if the corresponding steering behavior is "On"
	bool IsSeekOn(){ return On(seek); }
	bool IsFleeOn(){ return On(flee); }
	bool IsPursuitOn(){ return On(pursuit); }
	bool IsEvadeOn(){ return On(evade); }
	bool IsArriveOn(){ return On(arrive); }
	bool IsInterposeOn(){ return On(interpose); }
	bool IsWanderOn(){ return On(wander); }
	bool IsWanderYawOn(){ return On(wanderyaw); }
	bool IsWanderPitchOn(){ return On(wanderpitch); }
	bool IsCohesionOn(){ return On(cohesion); }
	bool IsSeparationOn(){ return On(separation); }
	bool IsAlignmentOn(){ return On(allignment); }

	bool IsCohesionUnitOn(){ return On(cohesionUnit); }
	bool IsSeparationUnitOn(){ return On(separationUnit); }
	bool IsAlignmentUnitOn(){ return On(allignmentUnit); }

	// Getters for wander variables
	float GetWanderJitter() const{ return m_WanderJitter; }
	float GetWanderDistance() const{ return m_WanderDistance; }
	float GetWanderRadius() const{ return m_WanderRadius; }

	// Getters for flocking weights
	float GetCohesionWeight() const{ return m_WeightCohesion; }
	float GetSeparationWeight() const{ return m_WeightSeparation; }
	float GetAlignmentWeight() const{ return m_WeightAlignment; }

	// Begin 3D Steering Behaviors

	FVector Seek(const FVector seekPoint);
	FVector Flee(const FVector fleePoint);
	FVector Arrive(const FVector arrivePoint, Deceleration deceleration);
	FVector Pursuit(Mover* otherCharacter);
	FVector Evade(Mover* otherCharacter);
	FVector Interpose(Mover* otherChar1, Mover* otherChar2);

	// Wander algorithm using a projected sphere in order to pick a wander point
	FVector Wander(float wanderRadius, float wanderDistance, float wanderJitter);

	// Wander algorithm using a projected circle on the X,Y plane
	FVector WanderYaw(float wanderRadius, float wanderDistance, float wanderJitter);

	// Wander algorithm using a projected circle on the X,Z plane
	FVector WanderPitch(float wanderRadius, float wanderDistance, float wanderJitter);

	FVector Cohesion(const TArray<Mover*> &neighbors);
	FVector Separation(const TArray<Mover*> &neighbors);
	FVector Alignment(const TArray<Mover*> &neighbors);

	FVector CohesionUnit(Mover* otherCharacter, const TArray<Mover*> &neighbors);
	FVector SeparationUnit(Mover* otherCharacter, const TArray<Mover*> &neighbors);
	FVector AlignmentUnit(Mover* otherCharacter, const TArray<Mover*> &neighbors);

	// End 3D Steering Behaviors

	//FVector2D Seek2D(const Mover* thisCharacter, const FVector2D seekPoint);

};
