// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseState.h"

class AFishAIController;

/**
 *	Global state for the Fish AI
 */
class UNDERWATERGAME_API FishGlobalState : public BaseState<AFishAIController>
{
protected:
	ACharacter* m_PlayerCharacter;

public:
	FishGlobalState(AFishAIController* owner);
	~FishGlobalState();

	virtual FString GetStateName() override;

	virtual void EnterState() override;
	virtual void LeaveState() override;

	virtual void Tick(float deltaTime) override;

protected:
	bool AcquirePlayerPtr();
};
