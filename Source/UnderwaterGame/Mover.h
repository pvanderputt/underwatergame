//

#pragma once

#include "MovingEntity.h"
#include "SteeringBehaviors.h"

/**
 *	
 */
class UNDERWATERGAME_API Mover : public MovingEntity
{
protected:
	SteeringBehavior* m_Steering;

	float m_TimeElapsed;

public:
	Mover();
	Mover(ACharacter* character, float maxForce, float maxTurnRate);
	~Mover();

	virtual void Tick(float deltaTime) override;

	float GetTimeElapsed(){ return m_TimeElapsed; }

	TArray<Mover*> GetNeighbors();
	TArray<Mover*> GetSchoolNeighbors();

	SteeringBehavior* Steer(){ return m_Steering; }
};