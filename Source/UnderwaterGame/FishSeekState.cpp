// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishSeekState.h"
#include "FishAIController.h"
#include "UnderwaterCharacter.h"

#include "FishIdleState.h"
#include "FishFleeState.h"

FishSeekState::FishSeekState(AFishAIController* owner) : BaseState(owner)
{
	m_PlayerCharacter = nullptr;
}

FishSeekState::~FishSeekState()
{
}

FString FishSeekState::GetStateName()
{
	return FString("FishSeekState");
}

void FishSeekState::EnterState()
{
	AcquirePlayerPtr();

	if (m_PlayerCharacter)
	{
		m_Owner->GetMover()->Steer()->PursuitOn(Cast<AUnderwaterCharacter>(m_PlayerCharacter)->GetMover());
	}
}

void FishSeekState::LeaveState()
{
	if (m_PlayerCharacter)
	{
		m_Owner->GetMover()->Steer()->PursuitOff();
	}

	m_PlayerCharacter = nullptr;
}

void FishSeekState::Tick(float deltaTime)
{
	// If we were unable to find the player character, then try to acquire it again.
	if (!m_PlayerCharacter)
	{
		// If we can't find the player again, then don't do anything
		if (!AcquirePlayerPtr())
			return;
	}

	float distanceToPlayer = FVector().Dist(m_Owner->GetCharacter()->GetActorLocation(), m_PlayerCharacter->GetActorLocation());
	if (distanceToPlayer < 200)
	{
		m_Owner->m_FishFSM->ChangeState(new FishFleeState(m_Owner));
	}
	else
	{
		//// Get the owning ACharacter
		//ACharacter* ownerCharacter = m_Owner->GetCharacter();
		//// Find the flee vector
		//FVector seekVector = SteeringBehavior::Seek(ownerCharacter, m_PlayerCharacter->GetActorLocation());
		//// Apply the vector to the character in this timestep
		////ownerCharacter->GetMovementComponent()->AddInputVector(seekVector * deltaTime);
		//ownerCharacter->AddMovementInput(seekVector, deltaTime, true);
		//ownerCharacter->SetActorRotation(ownerCharacter->GetVelocity().Rotation());
	}
}

bool FishSeekState::AcquirePlayerPtr()
{
	UWorld* world = m_Owner->GetWorld();
	if (world)
	{

		APlayerController* pController = world->GetFirstPlayerController();
		if (pController)
		{
		
			m_PlayerCharacter = pController->GetCharacter();
			if (m_PlayerCharacter)
			{
				return true;
			}
			else
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerCharacter."));
				}
				m_PlayerCharacter = nullptr;
				return false;
			}
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerController."));
			}
			m_PlayerCharacter = nullptr;
			return false;
		}
	}
	else
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find World."));
		}
		m_PlayerCharacter = nullptr;
		return false;
	}
	return false;
}