// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishAIController.h"
#include "FishCharacter.h"

#include "FishFleeState.h"
#include "FishIdleState.h"
#include "FishGlobalState.h"
#include "FishWanderState.h"
#include "FishFlockState.h"
#include "FishReturnHomeState.h"


AFishAIController::AFishAIController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	m_MaxForce = 400.f;
	m_MaxTurnRate = 500.f;
	m_MaxNeighborDistance = 750.f;
}

void AFishAIController::BeginPlay()
{
	Super::BeginPlay();

}

void AFishAIController::Possess(APawn* inPawn)
{
	Super::Possess(inPawn);

	// Create the Mover here so we know that the Character was connected to the Controller
	m_Mover = new Mover(GetCharacter(), m_MaxForce, m_MaxTurnRate);

	// Create the FSM after the Mover
	m_FishFSM = new StateMachine<AFishAIController>(this);
	m_FishFSM->SetGlobalState(new FishGlobalState(this));
	m_FishFSM->SetCurrentState(new FishWanderState(this));
}

void AFishAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	m_FishFSM->Tick(DeltaSeconds);

	m_Mover->Tick(DeltaSeconds);
	
}

void AFishAIController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	m_FishFSM->LeaveCurrentState();
	m_FishFSM->LeaveGlobalState();
	delete m_FishFSM;

	delete m_Mover;
}

TArray<Mover*> AFishAIController::GetNeighbors()
{
	TArray<Mover*> fishArray;
	//for (AActor* actor : GetLevel()->Actors)
	//{
	//	if (actor)
	//	{
	//		if (actor->GetClass() == this->GetClass())
	//		{
	//			fishArray.Add(actor);
	//			numInArray++;
	//		}
	//	}
	//}

	for (TActorIterator<AFishAIController> actorItr(GetWorld()); actorItr; ++actorItr)
	{
		// if the neighbor is within the max neighbor distance, add it to the list of neighbors
		// Use DistSq for speed
		// Also make sure that the fish don't flock with the player's school of fish
		if ((FVector::DistSquared(GetCharacter()->GetActorLocation(), actorItr->GetCharacter()->GetActorLocation()) <=  m_MaxNeighborDistance * m_MaxNeighborDistance) && 
			(actorItr->GetCurrentStateName() != "FishFlockState"))
		{
			fishArray.Add(actorItr->GetMover());
		}
	}

	return fishArray;
}

TArray<Mover*> AFishAIController::GetSchoolNeighbors()
{
	TArray<Mover*> fishArray;

	for (TActorIterator<AFishAIController> actorItr(GetWorld()); actorItr; ++actorItr)
	{
		// if the neighbor is within the max neighbor distance, add it to the list of neighbors
		// Use DistSq for speed
		// Also make sure that the fish only flock with other fish in the player school
		if ((FVector::DistSquared(GetCharacter()->GetActorLocation(), actorItr->GetCharacter()->GetActorLocation()) <= m_MaxNeighborDistance * m_MaxNeighborDistance) &&
			(actorItr->GetCurrentStateName() == "FishFlockState"))
		{
			fishArray.Add(actorItr->GetMover());
		}
	}

	return fishArray;
}

void AFishAIController::FlockWithPlayer()
{
	m_FishFSM->ChangeState(new FishFlockState(this));
}

void AFishAIController::SwimHome(FVector swimLocation)
{
	m_Mover->Steer()->SetSeekTarget(swimLocation);

	m_FishFSM->ChangeState(new FishReturnHomeState(this));
}

void AFishAIController::LeavePlayerFlock()
{
	m_FishFSM->ChangeState(new FishWanderState(this));
}

FString AFishAIController::GetCurrentStateName()
{
	return m_FishFSM->GetCurrentStateName();
}