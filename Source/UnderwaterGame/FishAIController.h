// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "StateMachine.h"
#include "Mover.h"

#include "FishAIController.generated.h"

/**
 * 
 */
UCLASS()
class UNDERWATERGAME_API AFishAIController : public AAIController
{
	GENERATED_UCLASS_BODY()

public:

	// Max force the AI can use to move
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float m_MaxForce;

	// Max Turn rate of the AI
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float m_MaxTurnRate;

	// The Max distance a neighboring fish can be away
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float m_MaxNeighborDistance;

	// mover for the AI, uses steering behaviors
	Mover* m_Mover;

	// State machine for the fish AI
	StateMachine<AFishAIController>* m_FishFSM;

public:
	virtual void BeginPlay() override;

	virtual void Possess(APawn* inPawn) override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	TArray<Mover*> GetNeighbors();
	TArray<Mover*> GetSchoolNeighbors();

	float GetMaxForce(){ return m_MaxForce; }
	float GetMaxTurnRate(){ return m_MaxTurnRate; }
	float GetMaxNeighborDistance(){ return m_MaxNeighborDistance; }

	Mover* GetMover(){ return m_Mover; }

	StateMachine<AFishAIController>* GetFSM(){ return m_FishFSM; }

public:
	// Called when the fish gets the signal to flock with the player
	UFUNCTION(BlueprintCallable, Category = AI)
	void FlockWithPlayer();

	UFUNCTION(BlueprintCallable, Category = AI)
	void SwimHome(const FVector swimLocation);

	void LeavePlayerFlock();

	UFUNCTION(BlueprintCallable, Category = AI)
	FString GetCurrentStateName();
};