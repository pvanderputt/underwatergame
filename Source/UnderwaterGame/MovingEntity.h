//

#pragma once

#include "BaseGameEntity.h"

#include "Math/Vector.h"
#include "Math/Vector2D.h"

/**
*
*/
class UNDERWATERGAME_API MovingEntity : public BaseGameEntity
{
protected:
	float m_MaxForce;
	float m_MaxTurnRate;

public:
	MovingEntity(){}
	MovingEntity(ACharacter* character,
		float maxForce,
		float maxTurnRate) : BaseGameEntity(character),
		m_MaxForce(maxForce),
		m_MaxTurnRate(maxTurnRate){}

	virtual ~MovingEntity(){}

	/** Getters **/
	FVector GetVelocity(){ return m_Character->GetVelocity(); }
	FVector GetHeading(){ return m_Character->GetTransform().GetUnitAxis(EAxis::X); }
	float GetMass(){ return 1; }
	float GetMaxSpeed(){ return m_Character->GetMovementComponent()->GetMaxSpeed(); }
	float GetMaxForce(){ return m_MaxForce; }
	float GetMaxTurnRate(){ return m_MaxTurnRate; }
	float GetSpeed(){ return m_Character->GetVelocity().Size(); }
	float GetSpeedSq(){ return m_Character->GetVelocity().SizeSquared(); }
};