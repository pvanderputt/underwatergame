// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishIdleState.h"
#include "FishAIController.h"

#include "FishFleeState.h"
#include "FishSeekState.h"

FishIdleState::FishIdleState(AFishAIController* owner) : BaseState(owner)
{
}

FishIdleState::~FishIdleState()
{
}

FString FishIdleState::GetStateName()
{
	return FString("FishIdleState");
}

void FishIdleState::EnterState()
{
}

void FishIdleState::LeaveState()
{
}

void FishIdleState::Tick(float deltaTime)
{
	//m_Owner->m_FishFSM->ChangeState(new FishFleeState(m_Owner));
}