// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "SteeringBehaviors.h"

#include "Mover.h"

SteeringBehavior::SteeringBehavior(Mover* owner):
		m_Owner(owner),
		m_BehaviorFlags(0),
		m_WeightArrive(1.0f),
		m_WeightEvade(1.0f),
		m_WeightFlee(1.0f),
		m_WeightInterpose(1.0f),
		m_WeightPursuit(1.0f),
		m_WeightSeek(1.0f),
		m_WeightWander(0.07f),

		m_WeightCohesion(12.0f),
		m_WeightSeparation(0.4f),
		m_WeightAlignment(15.0f),

		m_WeightCohesionUnit(0.3f),
		m_WeightSeparationUnit(500.0f),
		m_WeightAlignmentUnit(50.0f),

		m_WanderDistance(2000.0f),
		m_WanderRadius(950.0f),
		m_WanderJitter(4000.0f),
		m_Deceleration(normal),
		m_SummingMethod(prioritized)
{
	m_WanderTarget.Set(0.f, 0.f, 0.f);
}

SteeringBehavior::~SteeringBehavior()
{
}

FVector SteeringBehavior::Calculate()
{
	m_SteeringForce.Set(0.f, 0.f, 0.f);

	switch (m_SummingMethod)
	{
	case weighted_average:
		m_SteeringForce = CalculateWeightedSum();
		break;

	case prioritized:
		m_SteeringForce = CalculatePrioritized();
		break;

	case dithered:
		m_SteeringForce = CalculateDithered();
		break;

	default:
		m_SteeringForce = FVector(0.f, 0.f, 0.f);
		break;
	}

	VerifyGoodForce(m_SteeringForce);

	return m_SteeringForce;
}

bool SteeringBehavior::VerifyGoodForce(FVector &vector)
{
	bool goodVector = true;;
	// Check to make sure the vector doesn't contain any undefined numbers
	if (vector.X != vector.X)
	{
		vector.X = 0.0f;
		goodVector = false;
	}
	if (vector.Y != vector.Y)
	{
		vector.Y = 0.0f;
		goodVector = false;
	}
	if (vector.Z != vector.Z)
	{
		vector.Z = 0.0f;
		goodVector = false;
	}
	return goodVector;
}

bool SteeringBehavior::AccumulateForce(FVector &totalForce, FVector forceToAdd)
{
	// calculate how much force has been used so far
	float magnitudeSoFar = totalForce.Size();

	// calculate how much force is remaining
	float magnitudeRemaining = m_Owner->GetMaxForce() - magnitudeSoFar;

	// return false if there is no more force to be used
	if (magnitudeRemaining <= 0.f)
		return false;

	// calculate the magnitude of the force we want to add
	float magnitudeToAdd = totalForce.Size();

	// If the magnitude of the adding force doesn't exceed the magnitude remaining,
	// Add it to the total force
	if (magnitudeToAdd < magnitudeRemaining)
	{
		totalForce += forceToAdd;
	}
	else
	{
		// add the remainder to the steering force otherwise
		totalForce += (forceToAdd.SafeNormal() * magnitudeRemaining);
	}

	return true;
}

FVector SteeringBehavior::CalculateWeightedSum()
{
	// If we are using any of the flocking behaviors,
	// get a list of the neighbors only once throughout this cycle
	TArray<Mover*> neighbors;
	if (On(separation) || On(cohesion) || On(allignment) || On(separationUnit) || On(cohesionUnit) || On(allignmentUnit))
	{
		neighbors = m_Owner->GetNeighbors();
	}

	if (On(evade))
	{
		if (m_TargetChar1)
			m_SteeringForce += Evade(m_TargetChar1) * m_WeightEvade;
	}

	if (On(separation))
	{
		m_SteeringForce += Separation(neighbors) * m_WeightSeparation;
	}

	if (On(allignment))
	{
		m_SteeringForce += Alignment(neighbors) * m_WeightAlignment;;
	}

	if (On(cohesion))
	{
		m_SteeringForce += Cohesion(neighbors) * m_WeightCohesion;
	}

	if (On(wander))
	{
		m_SteeringForce += Wander(m_WanderRadius, m_WanderDistance, m_WanderJitter) * m_WeightWander;
	}

	if (On(wanderyaw))
	{
		m_SteeringForce += WanderYaw(m_WanderRadius, m_WanderDistance, m_WanderJitter) * m_WeightWander;
	}

	if (On(wanderpitch))
	{
		m_SteeringForce += WanderPitch(m_WanderRadius, m_WanderDistance, m_WanderJitter) * m_WeightWander;
	}

	if (On(seek))
	{
		m_SteeringForce += Seek(m_SeekTarget) * m_WeightSeek;
	}

	if (On(flee))
	{
		m_SteeringForce += Flee(m_FleeTarget) * m_WeightFlee;
	}

	if (On(arrive))
	{
		m_SteeringForce += Arrive(m_SeekTarget, m_Deceleration) * m_WeightArrive;
	}

	if (On(pursuit))
	{
		if (m_TargetChar1)
			m_SteeringForce += Pursuit(m_TargetChar1) * m_WeightPursuit;
	}

	if (On(interpose))
	{
		if (m_TargetChar1 && m_TargetChar2)
			m_SteeringForce += Interpose(m_TargetChar1, m_TargetChar2);
	}

	m_SteeringForce.Normalize();
	m_SteeringForce *= m_Owner->GetMaxForce();

	return m_SteeringForce;
}

FVector SteeringBehavior::CalculatePrioritized()
{
	FVector force;
	
	// If we are using any of the flocking behaviors,
	// get a list of the neighbors only once throughout this cycle
	TArray<Mover*> neighbors;
	if (On(separation) || On(cohesion) || On(allignment))
	{
		neighbors = m_Owner->GetNeighbors();
	}
	else if (On(separationUnit) || On(cohesionUnit) || On(allignmentUnit))
	{
		neighbors = m_Owner->GetSchoolNeighbors();
	}

	if (On(evade))
	{
		if (m_TargetChar1)
			force = Evade(m_TargetChar1) * m_WeightEvade;

		float x, y, z;
		x = force.X;
		y = force.Y;
		z = force.Z;
		
		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(flee))
	{
		force = Flee(m_FleeTarget) * m_WeightFlee;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(separationUnit))
	{
		force = SeparationUnit(m_TargetChar1, neighbors) * m_WeightSeparationUnit;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(cohesionUnit))
	{
		force = CohesionUnit(m_TargetChar1, neighbors) * m_WeightCohesionUnit;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(allignmentUnit))
	{
		force = AlignmentUnit(m_TargetChar1, neighbors) * m_WeightAlignment;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(separation))
	{
		force = Separation(neighbors) * m_WeightSeparation;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(allignment))
	{
		force = Alignment(neighbors) * m_WeightAlignment;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(cohesion))
	{
		force = Cohesion(neighbors) * m_WeightCohesion;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(seek))
	{
		force = Seek(m_SeekTarget) * m_WeightSeek;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(arrive))
	{
		force = Arrive(m_SeekTarget, m_Deceleration) * m_WeightArrive;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(wander))
	{
		force = Wander(m_WanderRadius, m_WanderDistance, m_WanderJitter) * m_WeightWander;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(wanderyaw))
	{
		force = WanderYaw(m_WanderRadius, m_WanderDistance, m_WanderJitter) * m_WeightWander;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(wanderpitch))
	{
		force = WanderPitch(m_WanderRadius, m_WanderDistance, m_WanderJitter) * m_WeightWander;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(pursuit))
	{
		if (m_TargetChar1)
			force = Pursuit(m_TargetChar1) * m_WeightPursuit;

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	if (On(interpose))
	{
		if (m_TargetChar1 && m_TargetChar2)
			force = Interpose(m_TargetChar1, m_TargetChar2);

		if (!AccumulateForce(m_SteeringForce, force))
			return m_SteeringForce;
	}

	return m_SteeringForce;
}

FVector SteeringBehavior::CalculateDithered()
{
	return FVector();
}


/////////////////////////////////////////////////////////////////////////
////
////				BEGIN STEERING BEHAVIORS
////
////////////////////////////////////////////////////////////////////////

FVector SteeringBehavior::Seek(const FVector seekPoint)
{
	FVector desiredVelocity = (seekPoint - m_Owner->GetPosition());
	desiredVelocity.Normalize();
	desiredVelocity = desiredVelocity * m_Owner->GetMaxSpeed();
	return (desiredVelocity - m_Owner->GetVelocity());
}

FVector SteeringBehavior::Flee(const FVector fleePoint)
{
	return -Seek(fleePoint);
}

FVector SteeringBehavior::Arrive(const FVector arrivePoint, Deceleration deceleration)
{
	// Get the vector from our location to the target
	FVector toTarget = arrivePoint - m_Owner->GetPosition();

	// Get the distance to the target
	float distanceToTarget = toTarget.Size();

	if (distanceToTarget > 0.f)
	{
		// Tweak value because our Deceleration param is an int
		const float decelerationTweak = 0.3f;

		// Calculate the speed needed to reach our target
		// Speed will lower the closer we are to the target
		float speed = distanceToTarget / ((float)deceleration* decelerationTweak);
		
		// Make sure speed doesn't go too high
		if (speed > m_Owner->GetMaxSpeed())
		{
			speed = m_Owner->GetMaxSpeed();
		}

		// Proceed like this were the seek function
		FVector desiredVelocity = toTarget * speed / distanceToTarget;

		return (desiredVelocity - m_Owner->GetVelocity());
	}

	return FVector(0.f, 0.f, 0.f);
}

FVector SteeringBehavior::Pursuit(Mover* otherCharacter)
{
	// if the target is coming towards us, seek the target's position
	FVector toTarget = otherCharacter->GetPosition() - m_Owner->GetPosition();

	// find out if the target is facing us and in front of us
	float relativeHeading = m_Owner->GetHeading()|otherCharacter->GetHeading();
	// if the target is facing us and we're facing it, seek it's location
	if ((toTarget | m_Owner->GetHeading()) > 0.f && (relativeHeading < -0.7)) // acos(0.7) ~ 45 degrees
	{
		return Seek(otherCharacter->GetPosition());
	}

	// distanceAhead is proportional to the distance between the two characters 
	// and is inversely proportional to the sum of their velocities
	float distanceAhead = toTarget.Size() / m_Owner->GetMaxSpeed() + otherCharacter->GetVelocity().Size();

	// Seek the predicted position of the other character
	return Seek(otherCharacter->GetPosition() + otherCharacter->GetVelocity() * distanceAhead * m_Owner->GetTimeElapsed());
}

FVector SteeringBehavior::Evade(Mover* otherCharacter)
{
	FVector toPursuer = otherCharacter->GetPosition() - m_Owner->GetPosition();

	// distanceAhead is proportional to the distance between the two characters 
	// and is inversely proportional to the sum of their velocities
	float distanceAhead = toPursuer.Size() / (m_Owner->GetMaxSpeed() + otherCharacter->GetVelocity().Size());

	// Flee from the predicted position of the other character
	return Flee(otherCharacter->GetPosition() + otherCharacter->GetVelocity() * distanceAhead * m_Owner->GetTimeElapsed());
}

FVector SteeringBehavior::Interpose(Mover* otherChar1, Mover* otherChar2)
{
	// Find the midpoint between two characters at this current time
	FVector midpoint = (otherChar1->GetPosition() + otherChar2->GetPosition()) / 2.f;

	// find out the time it would take us to reach the midpoint
	float timeToReachMidPoint = (midpoint - m_Owner->GetPosition()).Size() / m_Owner->GetMaxSpeed();

	// now find the projected points of where the two characters will be in that amount of time
	FVector pos1 = otherChar1->GetPosition() + otherChar1->GetVelocity() * timeToReachMidPoint;
	FVector pos2 = otherChar2->GetPosition() + otherChar2->GetVelocity() * timeToReachMidPoint;

	// calculate the midpoint between these predicted positions
	midpoint = (pos1 + pos2) / 2.f;

	// arrive at the point
	return Arrive(midpoint, fast);
}

// Inline function used for Wander method randomness
inline float RandomWanderAxis(){ return FMath::FRandRange(-1.f, 1.f); }
/**
 *	Wander
 *	This is the 3D version of wander, and projects a sphere to pick a random point to move to.
 *	This will likely make the AI seem drunk.
 */
FVector SteeringBehavior::Wander(float wanderRadius, float wanderDistance, float wanderJitter)
{
	float jitterThisTimeSlice = wanderJitter * m_Owner->GetTimeElapsed();

	// add a small random vector to the target position
	m_WanderTarget += FVector(RandomWanderAxis() * jitterThisTimeSlice,
		RandomWanderAxis() * jitterThisTimeSlice,
		RandomWanderAxis() * jitterThisTimeSlice);

	// normalize the vector so we have a direction
	m_WanderTarget.Normalize();

	// increase the length of the vector to the size of the radius
	m_WanderTarget *= wanderRadius;

	// project the vector out in front of the actor
	FVector target = m_Owner->GetPosition() +
		(m_Owner->GetVelocity().SafeNormal() * wanderDistance) +
		m_WanderTarget;

	// move towards it
	return Seek(target);
}

/**
 *	WanderYaw
 *	This is a 2D version of wander, where the AI will wander on the X,Y plane.
 *	The AI should only move left/right while wandering with this function.
 */
FVector SteeringBehavior::WanderYaw(float wanderRadius, float wanderDistance, float wanderJitter)
{
	float jitterThisTimeSlice = wanderJitter * m_Owner->GetTimeElapsed();

	// add a small random vector to the target position
	m_WanderTarget += FVector(RandomWanderAxis() * jitterThisTimeSlice,
		RandomWanderAxis() * jitterThisTimeSlice,
		0.0f);

	// normalize the vector so we have a direction
	m_WanderTarget.Normalize();

	// increase the length of the vector to the size of the radius
	m_WanderTarget *= wanderRadius;

	// project the vector out in front of the actor
	FVector target = m_Owner->GetPosition() +
		(m_Owner->GetVelocity().SafeNormal() * wanderDistance) +
		m_WanderTarget;

	// move towards it
	return Seek(target);
}

/**
 *	WanderPitch
 *	This is a 2D version of wander, where the AI will wander on the X,Z plane.
 *	The AI should only move up/down while wandering with this function.
 */
FVector SteeringBehavior::WanderPitch(float wanderRadius, float wanderDistance, float wanderJitter)
{
	float jitterThisTimeSlice = wanderJitter * m_Owner->GetTimeElapsed();

	// add a small random vector to the target position
	m_WanderTarget += FVector(RandomWanderAxis() * jitterThisTimeSlice,
		0.0f,
		RandomWanderAxis() * jitterThisTimeSlice);

	// normalize the vector so we have a direction
	m_WanderTarget.Normalize();

	// increase the length of the vector to the size of the radius
	m_WanderTarget *= wanderRadius;

	// project the vector out in front of the actor
	FVector target = m_Owner->GetPosition() +
		(m_Owner->GetVelocity().SafeNormal() * wanderDistance) +
		m_WanderTarget;

	// move towards it
	return Seek(target);
}

/**
 *	Cohesion
 *	returns a steering force that attempts to move the actor closer towards the
 *	center of mass of nearby actors
 */
FVector SteeringBehavior::Cohesion(const TArray<Mover*> &neighbors)
{
	// First find the center of mass of all the neighbors
	FVector centerOfMass(0.0f, 0.0f, 0.0f), steeringForce(0.0f, 0.0f, 0.0f);

	int neighborCount = 0;

	// iterate through the list and sum all the positions
	for (auto neighbor : neighbors)
	{
		// Make sure that the current character isn't included in calculations and
		// that the character isn't an evade target
		if ((neighbor != m_Owner) && (neighbor != m_TargetChar1))
		{
			centerOfMass += neighbor->GetPosition();
			++neighborCount;
		}
	}

	if (neighborCount > 0)
	{
		// The center of mass is the average of the positions
		centerOfMass /= (float)neighborCount;

		// Seek the center
		steeringForce = Seek(centerOfMass);
	}

	// Return the force
	//return steeringForce;
	return steeringForce.SafeNormal();
}

/**
 *	Separation
 *	this function calculates a repelling force from other neighbors
 */
FVector SteeringBehavior::Separation(const TArray<Mover*> &neighbors)
{
	FVector steeringForce(0.0f, 0.0f, 0.0f);

	for (auto neighbor : neighbors)
	{
		// Make sure that the current character isn't included in calculations and
		// that the character isn't an evade target
		if ((neighbor != m_Owner) && (neighbor != m_TargetChar1))
		{
			FVector toActor = m_Owner->GetPosition() - neighbor->GetPosition();

			// scale the force inversely proportional to the actor's distance
			steeringForce += toActor.SafeNormal() / toActor.Size();
		}
	}

	return steeringForce;
}

/**
 *	Alignment
 *	this function caculates an aligning force with neighbors
 */
FVector SteeringBehavior::Alignment(const TArray<Mover*> &neighbors)
{
	// Find the average heading of all the neighbors
	FVector averageHeading(0.0f, 0.0f, 0.0f);

	int neighborCount = 0;

	// iterate through the neighbors and sum their directions
	for (auto neighbor : neighbors)
	{
		// Make sure that the current character isn't included in calculations and
		// that the character isn't an evade target
		if ((neighbor != m_Owner) && (neighbor != m_TargetChar1))
		{
			averageHeading += neighbor->GetHeading();

			++neighborCount;
		}
	}

	// if there were neighbors, average their vectors
	if (neighborCount > 0)
	{
		averageHeading /= (float)neighborCount;

		averageHeading -= m_Owner->GetHeading();
	}

	return averageHeading;
}

/**
 *	Cohesion to a Unit
 *	This function works very similarly to regular cohesion, but instead uses
 *	the specified character as the center of mass
 */
FVector SteeringBehavior::CohesionUnit(Mover* otherCharacter, const TArray<Mover*> &neighbors)
{
	// First find the center of mass of all the neighbors
	FVector centerOfMass(0.0f, 0.0f, 0.0f), steeringForce(0.0f, 0.0f, 0.0f), finalCenter(0.0f, 0.0f, 0.0f);

	int neighborCount = 0;

	// iterate through the list and sum all the positions
	for (auto neighbor : neighbors)
	{
		// Make sure that the current character isn't included in calculations and
		// that the character isn't an evade target
		if ((neighbor != m_Owner) && (neighbor != m_TargetChar1))
		{
			centerOfMass += neighbor->GetPosition();
			++neighborCount;
		}
	}

	if (neighborCount > 0)
	{
		// We want the unit to have cohesion with the specified unit most of all,
		// so average the unit's position and the neighbor center of mass
		finalCenter = centerOfMass.SafeNormal() + otherCharacter->GetPosition();

		// Seek the of all the objects
		steeringForce = Seek(finalCenter);
	}
	else
	{
		//If we have no neighbors then just use the Unit's position
		finalCenter = otherCharacter->GetPosition();

		steeringForce = Seek(finalCenter);
	}

	// Return the steering force only if the vector is large enough
	// Otherwise return a zero vector
	if (otherCharacter->GetSpeedSq() > 400.0f)
		return steeringForce;
	else
		return FVector(0.0f, 0.0f, 0.0f);
}

/**
 *	Separation to a unit
 *	This function creates a force that separates it from its neighbors, but more importantly
 *	keeps it separated from the specified character.
 */
FVector SteeringBehavior::SeparationUnit(Mover* otherCharacter, const TArray<Mover*> &neighbors)
{
	FVector steeringForce(0.0f, 0.0f, 0.0f);

	for (auto neighbor : neighbors)
	{
		// Make sure that the current character isn't included in calculations and
		// that the character isn't an evade target
		if ((neighbor != m_Owner) && (neighbor != m_TargetChar1))
		{
			FVector toActor = m_Owner->GetPosition() - neighbor->GetPosition();

			// scale the force inversely proportional to the actor's distance
			steeringForce += toActor.SafeNormal() / toActor.Size();
		}
	}

	// Finally, add a steering force away from the specified Unit
	FVector toUnit = m_Owner->GetPosition() - otherCharacter->GetPosition();

	steeringForce += (toUnit.SafeNormal() * 12.0f) / toUnit.Size();

	// Return the steering force only if the vector is large enough
	// Otherwise return a zero vector
	if (otherCharacter->GetSpeedSq() > 400.0f)
		return steeringForce;
	else
		return FVector(0.0f, 0.0f, 0.0f);
}

/**
 *	Alignment to a Unit
 *	This function creates a force that aligns the character to the
 *	specified character as well as nearby units
 */
FVector SteeringBehavior::AlignmentUnit(Mover* otherCharacter, const TArray<Mover*> &neighbors)
{
	// Initialize local variables
	FVector averageHeading(0.0f, 0.0f, 0.0f);
	int neighborCount = 0;

	// Find the average heading of all the neighbors

	// iterate through the neighbors and sum their directions
	for (auto neighbor : neighbors)
	{
		// Make sure that the current character isn't included in calculations and
		// that the character isn't an evade target
		if ((neighbor != m_Owner) && (neighbor != m_TargetChar1))
		{
			averageHeading += neighbor->GetHeading();

			++neighborCount;
		}
	}

	// Add the heading of the specified unit to the average
	averageHeading += otherCharacter->GetHeading() * m_WeightAlignmentUnit;

	// if there were neighbors, average their vectors
	if (neighborCount > 0)
	{
		averageHeading /= (float)neighborCount;

		averageHeading -= m_Owner->GetHeading();
	}

	// Return the steering force only if the vector is large enough
	// Otherwise return a zero vector
	if (otherCharacter->GetSpeedSq() > 400.0f)
		return averageHeading;
	else
		return FVector(0.0f, 0.0f, 0.0f);
}