// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseState.h"

class AFishAIController;

/**
*	Flock State for the FishAI
*	The fish will attempt to flock with the player
*/
class UNDERWATERGAME_API FishFlockState : public BaseState<AFishAIController>
{
protected:
	ACharacter* m_PlayerCharacter;

	float m_FallOffDistance;

public:
	FishFlockState(AFishAIController* owner);
	~FishFlockState();

	virtual FString GetStateName() override;

	virtual void EnterState() override;
	virtual void LeaveState() override;

	virtual void Tick(float deltaTime) override;

protected:
	bool AcquirePlayerPtr();
};
