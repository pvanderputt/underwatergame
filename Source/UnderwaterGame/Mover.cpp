//

#include "UnderwaterGame.h"
#include "Mover.h"

#include "FishAIController.h"


Mover::Mover()
{
}

Mover::Mover(ACharacter* character, float maxForce, float maxTurnRate) :
				MovingEntity(character, maxForce, maxTurnRate)
{
	m_Steering = new SteeringBehavior(this);
	m_TimeElapsed = 0.0f;
}

Mover::~Mover()
{
	delete m_Steering;
}

void Mover::Tick(float deltaTime)
{
	// Track the time elapsed this frame.
	m_TimeElapsed = deltaTime;

	// Calculate the combined forces of all the steering behaviors currently active
	FVector steeringForce;
	steeringForce = m_Steering->Calculate();

	// Acceleration = Force / Mass
	FVector acceleration = steeringForce / GetMass();

	// Add the acceleration as movement input
	m_Character->AddMovementInput(acceleration, deltaTime, true);

	// Face in the direction of travel if our speed isn't very slow
	// and update the heading
	if (GetSpeedSq() > 625.0f)
	{
		m_Character->SetActorRotation(m_Character->GetVelocity().Rotation());
	}
}

TArray<Mover*> Mover::GetNeighbors()
{
	// cast up from AController to AFishAIController, since we know that it is one
	return Cast<AFishAIController>(m_Character->GetController())->GetNeighbors();
}

TArray<Mover*> Mover::GetSchoolNeighbors()
{
	// cast up from AController to AFishAIController, since we know that it is one
	return Cast<AFishAIController>(m_Character->GetController())->GetSchoolNeighbors();
}