// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishFleeState.h"
#include "FishAIController.h"
#include "UnderwaterCharacter.h"

#include "FishIdleState.h"
#include "FishSeekState.h"

FishFleeState::FishFleeState(AFishAIController* owner) : BaseState(owner)
{
	m_PlayerCharacter = nullptr;
}

FishFleeState::~FishFleeState()
{
}

FString FishFleeState::GetStateName()
{
	return FString("FishFleeState");
}

void FishFleeState::EnterState()
{
	AcquirePlayerPtr();

	if (m_PlayerCharacter)
	{
		m_Owner->GetMover()->Steer()->EvadeOn(Cast<AUnderwaterCharacter>(m_PlayerCharacter)->GetMover());
	}
}

void FishFleeState::LeaveState()
{
	if (m_PlayerCharacter)
	{
		m_Owner->GetMover()->Steer()->EvadeOff();
	}

	m_PlayerCharacter = nullptr;
}

void FishFleeState::Tick(float deltaTime)
{
	// If we were unable to find the player character, then try to acquire it again.
	if (!m_PlayerCharacter)
	{
		// If we can't find the player again, then don't do anything
		if (!AcquirePlayerPtr())
			return;
	}

	float distanceToPlayer = FVector().Dist(m_Owner->GetCharacter()->GetActorLocation(), m_PlayerCharacter->GetActorLocation());
	if (distanceToPlayer > 2000)
	{
		m_Owner->m_FishFSM->ChangeState(new FishSeekState(m_Owner));
	}
	else
	{
		//// Get the owning ACharacter
		//ACharacter* ownerCharacter = m_Owner->GetCharacter();
		//// Find the flee vector
		//FVector fleeVector = SteeringBehavior::Evade(ownerCharacter, m_PlayerCharacter);
		//// Apply the vector to the character in this timestep
		////ownerCharacter->GetMovementComponent()->AddInputVector(fleeVector * deltaTime);
		//ownerCharacter->AddMovementInput(fleeVector, deltaTime, true);
		//ownerCharacter->SetActorRotation(ownerCharacter->GetVelocity().Rotation());
	}
}

bool FishFleeState::AcquirePlayerPtr()
{
	UWorld* world = m_Owner->GetWorld();
	if (world)
	{

		APlayerController* pController = world->GetFirstPlayerController();
		if (pController)
		{

			m_PlayerCharacter = pController->GetCharacter();
			if (m_PlayerCharacter)
			{
				return true;
			}
			else
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerCharacter."));
				}
				m_PlayerCharacter = nullptr;
				return false;
			}
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerController."));
			}
			m_PlayerCharacter = nullptr;
			return false;
		}
	}
	else
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find World."));
		}
		m_PlayerCharacter = nullptr;
		return false;
	}
	return false;
}