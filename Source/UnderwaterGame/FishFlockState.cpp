// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishFlockState.h"
#include "FishAIController.h"
#include "UnderwaterCharacter.h"

#include "FishFleeState.h"
#include "FishSeekState.h"
#include "FishIdleState.h"
#include "FishWanderState.h"

FishFlockState::FishFlockState(AFishAIController* owner) : BaseState(owner)
{
	m_PlayerCharacter = nullptr;

	m_FallOffDistance = 1500.0f;
}

FishFlockState::~FishFlockState()
{
}

FString FishFlockState::GetStateName()
{
	return FString("FishFlockState");
}

void FishFlockState::EnterState()
{
	// When we enter the state, get a pointer to the player character
	AcquirePlayerPtr();

	// Try to flock with the player
	if (m_PlayerCharacter)
	{
		m_Owner->GetMover()->Steer()->FlockingUnitOn(Cast<AUnderwaterCharacter>(m_PlayerCharacter)->GetMover());
		Cast<AUnderwaterCharacter>(m_PlayerCharacter)->AddFish();
	}
}

void FishFlockState::LeaveState()
{
	// Stop flocking with the player when we leave the state
	m_Owner->GetMover()->Steer()->FlockingUnitOff();

	if (m_PlayerCharacter)
	{
		Cast<AUnderwaterCharacter>(m_PlayerCharacter)->SubtractFish();
	}
}

void FishFlockState::Tick(float deltaTime)
{
	// If we were unable to find the player character, then try to acquire it again.
	if (!m_PlayerCharacter)
	{
		// If we can't find the player again, then don't do anything
		if (!AcquirePlayerPtr())
			return;
	}

	if (FVector::DistSquared(m_PlayerCharacter->GetActorLocation(), 
							m_Owner->GetCharacter()->GetActorLocation()) >= 
							m_FallOffDistance * m_FallOffDistance)
	{
		m_Owner->LeavePlayerFlock();
	}
}

bool FishFlockState::AcquirePlayerPtr()
{
	UWorld* world = m_Owner->GetWorld();
	if (world)
	{

		APlayerController* pController = world->GetFirstPlayerController();
		if (pController)
		{

			m_PlayerCharacter = pController->GetCharacter();
			if (m_PlayerCharacter)
			{
				return true;
			}
			else
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerCharacter."));
				}
				m_PlayerCharacter = nullptr;
				return false;
			}
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerController."));
			}
			m_PlayerCharacter = nullptr;
			return false;
		}
	}
	else
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find World."));
		}
		m_PlayerCharacter = nullptr;
		return false;
	}
	return false;
}