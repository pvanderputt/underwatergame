// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "UnderwaterCharacter.h"

#include "FishCharacter.h"
#include "FishAIController.h"


AUnderwaterCharacter::AUnderwaterCharacter(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Set turn rates for camera input
	m_BaseTurnRate = 45.f;
	m_BaseLookUpRate = 45.f;

	// Set it so that the character doesn't turn when the camera turns
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	CharacterMovement->bOrientRotationToMovement = true; // True sets character to move in the direction of input
	CharacterMovement->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	
	// Create a camera boom for the third person camera
	m_CameraBoom = PCIP.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("m_CameraBoom"));
	m_CameraBoom->AttachTo(RootComponent);
	m_CameraBoom->TargetArmLength = 500.0f; // The camera follows the character at this distance
	m_CameraBoom->bUsePawnControlRotation = true; // Rotates the boom arm based on the controller

	// Create the third person camera
	m_FollowCamera = PCIP.CreateDefaultSubobject<UCameraComponent>(this, TEXT("m_FollowCamera"));
	m_FollowCamera->AttachTo(m_CameraBoom, USpringArmComponent::SocketName); // attach the camera to the end of the boom
	m_FollowCamera->bUsePawnControlRotation = false; // camera doesn't rotate relative to the boom arm

	m_FishCallDistance = 1000.0f;

	m_NumFishInSchool = 0;
}

void AUnderwaterCharacter::BeginPlay()
{
	m_Mover = new Mover(this, 0.0f, 0.0f);
}

void AUnderwaterCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	delete m_Mover;
}

void AUnderwaterCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Setup keybindings
	check(InputComponent);
	InputComponent->BindAction("Action", IE_Pressed, this, &AUnderwaterCharacter::PerformAction);
	
	// Movement
	InputComponent->BindAxis("MoveForward", this, &AUnderwaterCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AUnderwaterCharacter::MoveRight);
	InputComponent->BindAxis("MoveUp", this, &AUnderwaterCharacter::MoveUp);

	// Handle turning with the mouse
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	// Handle turning using a controller
	InputComponent->BindAxis("TurnRate", this, &AUnderwaterCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUpRate", this, &AUnderwaterCharacter::LookUpAtRate);

}

void AUnderwaterCharacter::MoveForward(float value)
{
	if (Controller != NULL && value != 0.0f)
	{
		// figure out which way is forward
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0, rotation.Yaw, 0);

		// get the forward vector
		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(direction, value);
	}
}

void AUnderwaterCharacter::MoveRight(float value)
{
	if (Controller != NULL && value != 0.0f)
	{
		// figure out which way is forward
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0, rotation.Yaw, 0);

		// get the right vector
		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(direction, value);
	}
}

void AUnderwaterCharacter::MoveUp(float value)
{
	if (Controller != NULL && value != 0.0f)
	{
		// figure out which way is forward
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0, rotation.Yaw, 0);

		// get the up vector
		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Z);
		AddMovementInput(direction, value);
	}
}

void AUnderwaterCharacter::TurnAtRate(float rate)
{
	// final turn rate is based on how far our controller stick is moved from rest position
	AddControllerYawInput(rate * m_BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AUnderwaterCharacter::LookUpAtRate(float rate)
{
	// final turn rate is based on how far our controller stick is moved from rest position
	AddControllerPitchInput(rate * m_BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AUnderwaterCharacter::PerformAction()
{
	// Get all the fish within the distance of the call and 
	// have them flock with the player
	for (TActorIterator<AFishAIController> actorItr(GetWorld()); actorItr; ++actorItr)
	{
		// Only fish that are in the distance of the call will follow the player
		// Also make sure that any fish returning to home don't go back to follow the player
		if ((FVector::DistSquared(GetActorLocation(), actorItr->GetCharacter()->GetActorLocation()) <= m_FishCallDistance * m_FishCallDistance) 
			&& (actorItr->GetCurrentStateName() != "FishReturnHomeState"))
		{
			actorItr->FlockWithPlayer();
		}
	}
}

void AUnderwaterCharacter::AddFish()
{
	++m_NumFishInSchool;
}

void AUnderwaterCharacter::SubtractFish()
{
	--m_NumFishInSchool;

	if (m_NumFishInSchool < 0)
		m_NumFishInSchool = 0;
}

uint32 AUnderwaterCharacter::GetNumFishInSchool()
{
	return m_NumFishInSchool;
}