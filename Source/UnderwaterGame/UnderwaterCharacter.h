// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Mover.h"

#include "UnderwaterCharacter.generated.h"

/**
 * 
 */
UCLASS()
class UNDERWATERGAME_API AUnderwaterCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

	/** Camera boom to position the camera behind the character **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	TSubobjectPtr<class USpringArmComponent> m_CameraBoom;

	/** Third person following Camera **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	TSubobjectPtr<class UCameraComponent> m_FollowCamera;

	/** Base turn rate in degrees per second **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float m_BaseTurnRate;

	/** Base look up/down rate in degrees per second **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float m_BaseLookUpRate;

	// The distance a fish will hear the call to follow the character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	float m_FishCallDistance;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay)
	uint32 m_NumFishInSchool;

	// Give the player character a mover object so that AI can interact with it.
	Mover* m_Mover;

protected:

	/** Moves the character forward / backward **/
	void MoveForward(float value);

	/** Moves the character right / left **/
	void MoveRight(float value);

	/** Moves the character up / down **/
	void MoveUp(float value);

	/** 
	 *	Turns the camera left / right at the given rate 
	 *	@param rate  Normalized rate. 1.0 is 100% of the desired turn rate
	 */
	void TurnAtRate(float rate);

	/**
	 *	Turns the camera up / down at the given rate
	 *	@param rate  Normalized rate. 1.0 is 100% of the desired turn rate
	 */
	void LookUpAtRate(float rate);

	/**
	 *	Function is called when pressing the spacebar or bottom gamepad face button (PlayStation: "X"), (XBox: "A")
	 *	Character performs their action.
	 */
	void PerformAction();

protected:

	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:

	Mover* GetMover(){ return m_Mover; }

	void AddFish();

	void SubtractFish();

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	uint32 GetNumFishInSchool();

	

};
