// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseState.h"

class AFishAIController;

/**
*	Flee State for the FishAI
*	The fish will attempt to seek the player when he gets too far away.
*/
class UNDERWATERGAME_API FishSeekState : public BaseState<AFishAIController>
{
protected:
	// Pointer to the player character
	ACharacter* m_PlayerCharacter;

public:
	FishSeekState(AFishAIController* owner);
	~FishSeekState();

	virtual FString GetStateName() override;

	virtual void EnterState() override;
	virtual void LeaveState() override;

	virtual void Tick(float deltaTime) override;

protected:
	bool AcquirePlayerPtr();
};
