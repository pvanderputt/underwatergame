// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseState.h"

class AFishAIController;

/**
 *	Flee State for the FishAI
 *	The fish will attempt to flee the player when he gets too close.
 */
class UNDERWATERGAME_API FishFleeState : public BaseState<AFishAIController>
{
protected:
	// Pointer to the player character
	ACharacter* m_PlayerCharacter;

public:
	FishFleeState(AFishAIController* owner);
	~FishFleeState();

	virtual FString GetStateName() override;

	virtual void EnterState() override;
	virtual void LeaveState() override;

	virtual void Tick(float deltaTime) override;

protected:
	bool AcquirePlayerPtr();
};
