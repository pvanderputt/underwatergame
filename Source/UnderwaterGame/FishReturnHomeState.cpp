// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishReturnHomeState.h"
#include "FishAIController.h"

#include "FishFleeState.h"
#include "FishSeekState.h"
#include "FishIdleState.h"
#include "FishFlockState.h"

FishReturnHomeState::FishReturnHomeState(AFishAIController* owner) : BaseState(owner)
{
	m_PlayerCharacter = nullptr;
}

FishReturnHomeState::~FishReturnHomeState()
{
}

FString FishReturnHomeState::GetStateName()
{
	return FString("FishReturnHomeState");
}

void FishReturnHomeState::EnterState()
{
	m_Owner->GetMover()->Steer()->SeekOn();
}

void FishReturnHomeState::LeaveState()
{
	m_Owner->GetMover()->Steer()->SeekOff();
}

void FishReturnHomeState::Tick(float deltaTime)
{

}

bool FishReturnHomeState::AcquirePlayerPtr()
{
	UWorld* world = m_Owner->GetWorld();
	if (world)
	{

		APlayerController* pController = world->GetFirstPlayerController();
		if (pController)
		{

			m_PlayerCharacter = pController->GetCharacter();
			if (m_PlayerCharacter)
			{
				return true;
			}
			else
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerCharacter."));
				}
				m_PlayerCharacter = nullptr;
				return false;
			}
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerController."));
			}
			m_PlayerCharacter = nullptr;
			return false;
		}
	}
	else
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find World."));
		}
		m_PlayerCharacter = nullptr;
		return false;
	}
	return false;
}