// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "UGGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UNDERWATERGAME_API AUGGameMode : public AGameMode
{
	GENERATED_UCLASS_BODY()

public:
	virtual void BeginPlay() override;


};