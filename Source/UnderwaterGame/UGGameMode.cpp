// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "UGGameMode.h"
#include "UnderwaterCharacter.h"
#include "FishCharacter.h"


AUGGameMode::AUGGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	FMath::RandInit(rand());

	// set the default pawn class to the UnderwaterCharacter
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_UnderwaterCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	//static ConstructorHelpers::FClassFinder<AHUD> HUDClassBP(TEXT("/Game/Blueprints/GameHUD"));
	//if (HUDClassBP.Class != NULL)
	//{
	//	HUDClass = HUDClassBP.Class;
	//}
}

void AUGGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Using UGGameMode"));
	}

	/*static ConstructorHelpers::FClassFinder<AFishCharacter> FishCharBPClass(TEXT("/Game/Blueprints/BP_FishCharacter"));
	if (FishCharBPClass.Class != NULL)
	{
		TSubclassOf<AFishCharacter> FishCharacterBP = FishCharBPClass.Class;

		UWorld* const World = GetWorld();
		if (World)
		{
			FActorSpawnParameters spawnParams;
			spawnParams.Instigator = NULL;
			AFishCharacter* fishChar = World->SpawnActor<AFishCharacter>(FishCharacterBP, FVector(0.f, 0.f, 0.f), FRotator(), spawnParams);
		}
	}*/
}

