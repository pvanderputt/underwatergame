// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseState.h"

class AFishAIController;

/**
 *	Idle State for the FishAI
 *	The fish will idle around and not do anything in particular
 */
class UNDERWATERGAME_API FishIdleState : public BaseState<AFishAIController>
{
public:
	FishIdleState(AFishAIController* owner);
	~FishIdleState();

	virtual FString GetStateName() override;

	virtual void EnterState() override;
	virtual void LeaveState() override;

	virtual void Tick(float deltaTime) override;
};
