// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * BaseState - A templated basestate to be inherited off of for many different uses.
 */
template <class owner_type>
class UNDERWATERGAME_API BaseState
{
public:
	owner_type* m_Owner;

public:
	BaseState(owner_type* owner) : m_Owner(owner){}
	virtual ~BaseState(){}
	virtual FString GetStateName() = 0;

	virtual void EnterState() = 0;
	virtual void LeaveState() = 0;

	virtual void Tick(float deltaTime) = 0;
};