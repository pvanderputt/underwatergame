// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishGlobalState.h"
#include "FishAIController.h"

FishGlobalState::FishGlobalState(AFishAIController* owner) : BaseState(owner)
{
	m_PlayerCharacter = nullptr;
}

FishGlobalState::~FishGlobalState()
{
}

void FishGlobalState::EnterState()
{
	// Acquire a pointer to the player
	AcquirePlayerPtr();

	// Turn flocking behaviors on
	//m_Owner->GetMover()->Steer()->FlockingOn();
}

void FishGlobalState::LeaveState()
{
}

FString FishGlobalState::GetStateName()
{
	return FString("FishGlobalState");
}

void FishGlobalState::Tick(float deltaTime)
{
	// If we were unable to find the player character, then try to acquire it again.
	if (!m_PlayerCharacter)
	{
		// If we can't find the player again, then don't do anything
		if (!AcquirePlayerPtr())
			return;
	}

	//if (m_Owner->GetCharacter()->GetActorLocation().X > 8000.0f || m_Owner->GetCharacter()->GetActorLocation().Z > 8000.0f)
	//{
	//	m_Owner->GetMover()->Steer()->SetSeekTarget(FVector(0, 0, 0));
	//	m_Owner->GetMover()->Steer()->SeekOn();
	//}
}

bool FishGlobalState::AcquirePlayerPtr()
{
	UWorld* world = m_Owner->GetWorld();
	if (world)
	{

		APlayerController* pController = world->GetFirstPlayerController();
		if (pController)
		{

			m_PlayerCharacter = pController->GetCharacter();
			if (m_PlayerCharacter)
			{
				return true;
			}
			else
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerCharacter."));
				}
				m_PlayerCharacter = nullptr;
				return false;
			}
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerController."));
			}
			m_PlayerCharacter = nullptr;
			return false;
		}
	}
	else
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find World."));
		}
		m_PlayerCharacter = nullptr;
		return false;
	}
	return false;
}