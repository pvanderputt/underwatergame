// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseState.h"

/**
 * StateMachine - A templated finite state machine for many different uses
 */

template <class owner_type>
class UNDERWATERGAME_API StateMachine
{
public:
	owner_type* m_Owner;

	BaseState<owner_type>* m_CurrentState;
	BaseState<owner_type>* m_PreviousState;

	BaseState<owner_type>* m_GlobalState;

public:
	StateMachine(){}
	StateMachine(owner_type* owner) : m_Owner(owner), m_CurrentState(nullptr),
		m_PreviousState(nullptr), m_GlobalState(nullptr){}

	~StateMachine()
	{
		if (m_CurrentState)
		{
			m_CurrentState->LeaveState();
			delete m_CurrentState;
		}
		if (m_GlobalState)
		{
			m_GlobalState->LeaveState();
			delete m_GlobalState;
		}
		if (m_PreviousState)
		{
			delete m_PreviousState;
		}
	}

	void Tick(float deltaTime)
	{
		if (m_CurrentState)
		{
			m_CurrentState->Tick(deltaTime);
		}
		if (m_GlobalState)
		{
			m_GlobalState->Tick(deltaTime);
		}
	}

	void ChangeState(BaseState<owner_type>* state)
	{
		if (m_PreviousState)
		{
			delete m_PreviousState;
		}
		if (m_CurrentState)
		{
			m_CurrentState->LeaveState();
			SetPreviousState(m_CurrentState);
			SetCurrentState(state);
		}
	}

	void SetCurrentState(BaseState<owner_type>* state)
	{
		m_CurrentState = state;
		m_CurrentState->EnterState();
	}

	void LeaveCurrentState()
	{
		if (m_PreviousState)
		{
			delete m_PreviousState;
		}
		if (m_CurrentState)
		{
			m_CurrentState->LeaveState();
			SetPreviousState(m_CurrentState);
			m_CurrentState = nullptr;
		}
	}

	void ChangeGlobalState(BaseState<owner_type>* state)
	{
		LeaveGlobalState();
		SetGlobalState(state);
	}

	void SetGlobalState(BaseState<owner_type>* state)
	{
		m_GlobalState = state;
		m_GlobalState->EnterState();
	}

	void LeaveGlobalState()
	{
		if (m_GlobalState)
		{
			m_GlobalState->LeaveState();
			delete m_GlobalState;
			m_GlobalState = nullptr;
		}
	}

	void SetPreviousState(BaseState<owner_type>* state)
	{
		m_PreviousState = state;
	}

	FString GetCurrentStateName()
	{
		return m_CurrentState->GetStateName();
	}

	FString GetGlobalStateName()
	{
		return m_GlobalState->GetStateName();
	}
};