// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseState.h"

class AFishAIController;

/**
*	Wander State for the FishAI
*	The fish will wander around the world
*/
class UNDERWATERGAME_API FishReturnHomeState : public BaseState<AFishAIController>
{
protected:
	ACharacter* m_PlayerCharacter;

public:
	FishReturnHomeState(AFishAIController* owner);
	~FishReturnHomeState();

	virtual FString GetStateName() override;

	virtual void EnterState() override;
	virtual void LeaveState() override;

	virtual void Tick(float deltaTime) override;

protected:
	bool AcquirePlayerPtr();
};
