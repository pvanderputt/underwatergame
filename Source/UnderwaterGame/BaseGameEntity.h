//

#pragma once

#include "GameFramework/Character.h"


/**
 *	BaseGameEntity - The base class of any gameplay entity class.
 */
class UNDERWATERGAME_API BaseGameEntity
{
protected:
	// The Character that this object is connected to
	ACharacter* m_Character;

public:
	BaseGameEntity(){}
	BaseGameEntity(ACharacter* character) : m_Character(character){}

	virtual ~BaseGameEntity(){}

	virtual void Tick(float deltaTime){};

	FVector GetPosition(){ return m_Character->GetTransform().GetLocation(); }

	ACharacter* GetCharacter(){ return m_Character; }
};