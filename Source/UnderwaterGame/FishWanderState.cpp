// Fill out your copyright notice in the Description page of Project Settings.

#include "UnderwaterGame.h"
#include "FishWanderState.h"
#include "FishAIController.h"

#include "FishFleeState.h"
#include "FishSeekState.h"
#include "FishIdleState.h"
#include "FishFlockState.h"

FishWanderState::FishWanderState(AFishAIController* owner) : BaseState(owner)
{
	m_PlayerCharacter = nullptr;
}

FishWanderState::~FishWanderState()
{
}

FString FishWanderState::GetStateName()
{
	return FString("FishWanderState");
}

void FishWanderState::EnterState()
{
	// When we enter the state, get a pointer to the player character
	//AcquirePlayerPtr();

	// Begin wandering around
	m_Owner->GetMover()->Steer()->WanderOn();
	m_Owner->GetMover()->Steer()->FlockingOn();
}

void FishWanderState::LeaveState()
{
	// Stop wandering when we leave this state
	m_Owner->GetMover()->Steer()->WanderOff();
	m_Owner->GetMover()->Steer()->FlockingOff();
}

void FishWanderState::Tick(float deltaTime)
{
	//// If we were unable to find the player character, then try to acquire it again.
	//if (!m_PlayerCharacter)
	//{
	//	// If we can't find the player again, then don't do anything
	//	if (!AcquirePlayerPtr())
	//		return;
	//}
}

bool FishWanderState::AcquirePlayerPtr()
{
	UWorld* world = m_Owner->GetWorld();
	if (world)
	{

		APlayerController* pController = world->GetFirstPlayerController();
		if (pController)
		{

			m_PlayerCharacter = pController->GetCharacter();
			if (m_PlayerCharacter)
			{
				return true;
			}
			else
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerCharacter."));
				}
				m_PlayerCharacter = nullptr;
				return false;
			}
		}
		else
		{
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find PlayerController."));
			}
			m_PlayerCharacter = nullptr;
			return false;
		}
	}
	else
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Fish State Error: Could not find World."));
		}
		m_PlayerCharacter = nullptr;
		return false;
	}
	return false;
}